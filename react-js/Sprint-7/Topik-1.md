# Kompetensi
- Memahami cara yang lebih baik membuat form di React

# Materi

## Form Management

Ketika berbicara tentang form, pada umumnya yang ada dipikiran kita ada tiga area. _**Form Field Elements (representasi)**_,  _**Form Values (manajemen state)**_ dan _**Field Validations (validation)**_. Mari kita lihat apa yang bisa kita ambil dengan menjabarkan ketga aspek tersebut satu per satu.

### **Form State Management**
Developer React yang masih baru mungkin akan sedikit terkejut dengan betapa banyak pekerjaan manual yang harus dilakukan jika dibandingkan dengan menggunakan JQuery untuk mengambil value dari form. Di **JQuery** bisa menggunakan [serializeArray](https://api.jquery.com/serializeArray/) untuk mengembalikkan seluruh value dari form ke dalam bentuk array berisi objek.

Di React ada dua cara untuk mengelola hal tersebut. bisa melalui _controlled form component_ atau _uncontrolled form component_. Kita akan jelaskan dulu apa itu _controlled component_ dan _uncontrolled component_.

**Controlled Form Component**

Controlled Form Component adalah field input yang telah didefinisikan dengan properti `value`. Value dari input ini dikontrol oleh React, inputan dari user tidak akan memiliki pengaruh langsung pada input yang dirender. Input yang user berikan tidak langsung terender melainkan akan melalui menejemen state di React terlebih dulu.

```javascript
class Form extends React.Component {
  constructor(props) {
    super(props);

    this.state: {
      name: ''
    };
  }

  onChange = (e) => {
    this.setState({
      name: e.target.value
    });
  }

  render() {
    return(
      <div>
        <label>Nama : </label>
        <input
          id="name-input"
          onChange={this.onChange}
          value={this.state.name} />
      </div>
    )
  }
}
```
Contoh di atas mendemonstrasikan bagaimana properti `value` mendefinisikan value yang ada saat itu dan event handler `onChange` memperbarui state dengan inputan dari user.

Form input seharusnya didefinisikan sebagai controlled component jika memungkinkan. 

**Uncontrolled Form Component**

Uncontrolled Form Component adalah input yang tidak memiliki properti `value`. Sebaliknya dari controlled component, in adalah tanggung jawab aplikasi untuk menjaga state dan val;ue tetap sinkron.

```javascript
class Form extends React.Component {
  constructor(props) {
    super(props);

    this.state: {
      name: 'Richard'
    };
  }

  onChange = (e) => {
    this.setState({
      name: e.target.value
    });
  }

  render() {
    return(
      <div>
        <label>Nama : </label>
        <input
          id="name-input"
          onChange={this.onChange}
          defaultValue={this.state.name} />
      </div>
    )
  }
}
```

Ini dia, untuk state yang terupdate hanya untuk controlled component.
Bagaimanapun, ganti dari properti value adalah properti `defaultValue`. Ini menetapkan value awal dari input selama perenderan pertama. Perubahan selanjutnya pada state tidak terefleksi secara automatis oleh value input.

### **Form Validation**

Aspek penting lainnya yang perlu diperhatikan adalah validasi. Kita perlu yakin bahwa data yang diinputkan oleh user adalah data yang valid dan menampilkan pesan error apabila input yang dimasukkan oleh user tidak valid.

Contohnya seperti field username tidak boleh kosong, password harus lebih dari 6 karakter dll. Jika tidak sesuai validasi maka akan muncul pesan error.

Contoh fungsi untuk validasi

```javascript
const validate = (e) => {
  if(e.target.value.length === 0) {
    this.setState({
      error: "Field ini tidak boleh kosong"
    })
  }
}
```

Yang perlu kita lakukan sekarang adalah dapat menjalankan form yang tervalidasi dan menampilkan pesan errornya ketika terjadi suatu error. Berdasarkan fakta bahwa React merender komponen sesegera mungkin saat statenya berubah, kita bisa menjalankan validasinya setiap saat kita merender. Itu juga berarti bahwa kita tidak perlu menyimpan pesan error default di state.

Setiap kali kita merender, kita memvalidasi state form yang ada.

```jsx
<form>
  <label>
    First Name *:
    <input
      name="firstName"
      type="text"
      value={this.state.firstName}
      onKeyPress={this.validate}
    />
    {this.state.error && <span style={{color: "red"}}>{this.state.error}</span>}
  </label>
</form>
```

### **Form Representation**

Setelah membahas manajemen state dan aspek validasi, kita juga perlu menjaga representasi aktual untuk form kita. React memungkinkan kita untuk mendefinisikan komponen, menyusun komponen,komponen dll. Fakta terpenting ang perlu digaris bawahi adlah ahwa komponen kita seharusnya tidak tahu apapa tentang bagaimana keadaan formulir dikelola atau bagaimana state tervalidasi. Selama kita memisahkan representasi dari dua aspek tadi, kita dapat membangun komponen yang menerima props dan menampilkan informasi sesuai dengan props. Contohnya bisa dilihat [di sini](https://codesandbox.io/s/4q6j24zk2x?from-embed)

# Meta

kata kunci :
- Forms in react
- Form management in react

tautan :
- [Better Understanding Forms in React](https://medium.com/javascript-inside/better-understanding-forms-in-react-a85d889773ce)


# Latihan
Terapkan ketiga aspek di atas pada komponen login dan register yang telah dibuat sebelumnya.
