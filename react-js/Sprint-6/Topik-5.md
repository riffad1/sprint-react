# Kompetensi

# Materi

## **React UI Testing**

Ada banyak aspek yang perlu kita perhatikan jika ingin melakukan testing pada UI. Juga ada banyak peralatan dan teknik yang bisa kita gunakan.

**Alasan Melakukan Testing**

Sebelum kita berbicara soal testing, kita perlu memikirkan "kenapa" kita perlu testing tersebut. Ada berbagai alasan untuk menjawabnya, diantaranya :

- Untuk menemukan bug
- Untuk memastikan bahwa tidak terjadi crash saat ada commit baru
- Unutk menyimpan test sebagai dokumentasi.

Lebih spesifiknya testing cukup itu penting ketika bekerja dengan tim karena memungkinkan orang yang berbeda kemampuan untuk berkontribusi dengan percaya diri.

**Berbagai Aspek testing UI**

Kita merujuk pada kaya _UI_ ke dalam banyak hal. Untuk memokuskan hal ini, mari kita persempit ke React berbasis User Interface.

1. **Structural Testing**

Di sini kita kan fokus pada struktur dari UI dan bagaimana UI tersebut diletakkan. Untuk contohnya, kita anggap bahwa kita memiliki sebuah _login component_ seperti gambar di bawah:

![login component](login-component.png)

Untuk struktural testing, kita akan menguji apakah komponen tersebut memiliki konten berikut atau tidak :

- Sebuah judul "Log in to Facebook".
- Dua input untuk username dan password.
- Tombol submit.
- Pemberitahuan jika terjadi error.

Untuk React, selama ini yang biasa digunakan untuk melakukan struktural testing adalah dengan [Enzyme](https://github.com/airbnb/enzyme), tapi sekarang kita bisa menggunakan [Jest's snapshot testing](https://jestjs.io/blog/2016/07/27/jest-14.html) untuk membuat segalanya lebih sederhana.

2. **Interaction Testing**

UI adalah segala hal tentang interaksi dengan pengguna. Kita melakukan ini dengan banyak elemen UI, seperti button, link, dan elemen input. Dengan testing interaksi, kita perlu menguji kalau elemen-elemen tersebut bekerja dengan baik.

Mari kita gunakan komponen login di atas sebagai contoh.

- Ketika kita meng-kilk tombol submit, maka akan terkirim value username dan password.
- Ketika kita meng-kilk ombol "Forgotten Account", maka halaman akan meredirect ke halaman baru.

Kita memiliki beberapa cara untuk memlalukan testing yang seperti ini, namun cara paling sederhana adalah menggunakan Enzyme.

3. **CSS/Style Testing**

UI adalah segala hal tentang style, entah dia itu sederhana, menarik atau bahkan buruk. Dengan style testing kita akan mengevaluasi _look and feel_ dari komponen UI kita antara perubahan kode. Ini merupakan subjek yang cukup kompleks dan biasanya kita melakukannya dengan membandingkan gambar.

Jika kita menggunakan style inline, kita bisa menggunakan JEST snapshot testing.

4. **Manual Testing**

Semua bagian di atas membicarakan tentang testing menggunakan tools automatis. Tapi karena kita membangun UI untuk manusia maka kita harus melakukan tes secara manual untuk bisa merasakan bagaimana yang orang lain akan rasakan.

Alasan lain untuk testing manual adalah supaya mendapatkan user ecperience yang lebih baik.

Kita harus selalu mencoba menguji UI kita dengan mata telanjang. Untuk kasus ini, kita bisa menggunakan simpel storybook yang sudah ada. 

# Meta
- [Enzyme Airbnb](https://airbnb.io/enzyme/) 
- [Storybook](https://storybook.js.org/testing/react-ui-testing/)

# Latihan
Kamu akan membuat sebuah aplikasi react yang memiliki UI login dan register. Tapi sebelum membuat aplikasi tersebut kamu harus melakukan testing terlebih dahulu untuk memastikan bahwa UI yang ada pada tampilanmu berjalan dengan baik.

[note] Rancang terlebih dahulu komponen apa saja yang dibutuhkan pada aplikasi yang akan kamu buat supaya bisa dilakukan pengujian UI.
