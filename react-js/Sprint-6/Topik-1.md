# Kompetensi

- Dapat membuat routing dengan react-router-dom v4
- Dapat membuat deklarasi routing pada folder routes
- Dapat mengimplementasikan routing di aplikasi web

# Materi

## React Router

**React Router 4** adalah tubuh/inti dari komponen navigasional yang menawarkan routing secara deklaratif pada aplikasi React. Pada materi kali ini akan dipaparkan mengenai cara menggunakan React Router.

## Pengenalan
_Routing_ atau dalam bahasa indonesianya adalah _Perutean_ merupakan satu hal yang sangat penting pada hampir setiap arsitektur aplikasi. Semakin besar suatu aplikasi, semakin kompleks pula fungsionalitas Peruteannya(Routingnya), dari Perutean yang sederhana hingga skenario Perutean(Routing) yang bercabang. 

**React Router** adalah library yang paling populer dan yang paling umum digunakan untuk melakukan Routing pada aplikasi React. Seiring pertumbuhan suatu aplikasi yang membutuhkan beberapa tampilan(view) dan rute(route), maka idealnya untuk memilih _Router_ yang bagus untuk membantu mengatur transisi atau perpindahan antara tampilan, redirect, mendapatkan paramaeter URL dengan mudah dll.

Dengan **React Router 4** kita dapat merutekan secara deklaratif. API **React Router 4** pada dasarnya hanya kumpulan komponen-komponen sehingga membuatnya mudah digunakan. 

## Pengaturan & Pemasangan

Yang dibutuhkan :

- Node.js (versi 6 keatas) dan NPM
- create-react-app untuk membuat project react

**React Router** tersusun atas: `react-router`, `react-router-dom` dan `react-router-native`

- **react-router**: meliputi inti perutean komponen
- **react-router-dom**: meliputi API perutean yang diperlukan untuk browser
- **react-router-native**: meliputi API perutean untuk aplikasi mobile

## Penggunaan

Buat proyek baru dengan _create-react-app_ dan navigasikan ke folder/direktori yang telah dibuat seperti dibawah ini: 

```
create-react-app base

cd base
```

Install `react-router-dom` .
```
npm install --save react-router-dom
```

Kita akan fokus pada penggunaan **React Router 4** untuk browser. Kita akan membahas konsep-konsep penting yang telah disusun di bawah ini :
- Basic Routing
- Basic Components
- Nested Routing and URL Parameters


=====================

## **Basic Component**
Ada 3 tipe komponen pada React Router: _router component_, _route matching component_  dan _navigation component_.

Semua komponen yang kamu gunakan harus diimpor dari `react-router-dom` .

```javascript
import { BrowserRouter, Route, Link } from 'react-router-dom'
```

### **Route Matching**
Ada dua komponen Route Matching, yaitu: `<Route>` dan `<Switch>`.

```javascript
import { Route, Switch } from 'react-router-dom';
```

Route matching dilakukan dengan membandingkan _prop_ jalur(path) `<Route>` dengan lokasi saat ini. Ketika `<Route>` telah cocok dengan lokasi saat ini maka ia akan me-_render_ kontennya dan ketika tidak cocok maka ia akan me-_render_ null. Sebuah `<Route>` tanpa jalur(path) akan selalu cocok.

```javascript
//ketika lokasi = { pathname: '/about' }
<Route path='/about' component={About}/> // renders <About />
<Route path='/contact' component={Contact}/> // renders null
<Route component={Always}/> // renders <Always />
```

Kamu bisa memasukkan `<Route>` dimanapun kamu ingin me-_render_ konten sesuai dengan lokasinya. 


Selanjutnya adalah `<Switch>`,  `<Switch>` merupakan komponen yang digunakan untuk mengelompokkan `<Route>` berasama-sama.

```javascript
<Switch>
  <Route exact path='/' component={Home} />
  <Route path="/about" component={About} />
  <Route path="/contact" component={Contact} />
</Switch>
```

Sebenarmya `<Switch>` tidak terlalu perlu digunakan untuk pengelompokan `<Route>` , tapi ia akan sedikit berguna nantinya. `<Switch>` akan meng-_iterasi_ semua anak elemen `<Route>`ya dan hanya me-_render_ komponen pertama yang cocok dengan lokasi saat ini.

Ini akan membantu ketika ada _multiple route's path_ yang cocok dengan nama jalur(path) yang sama, ketika aplikasi melakukan transisi antar _route_ dan pada saat pengidentifikasian tidak ditemukan _route_ yang cocok dengan lokasi saat itu maka kamu bisa merender "404 componnet".

```javascript
<Switch>
  <Route exact path="/" component={Home} />
  <Route path="/about" component={About} />
  <Route path="/contact" component={Contact} />
  
  // ketika tidak ada satupun rute di atas yang cocok, <NoMatch> akan di-_render_
  <Route component={NoMatch} />
</Switch>
```

### **Navigation**
React Router memiliki komponen `<Link>` untuk membuat link pada aplikasimu. Kapanpun kamu me-_render_ `<Link>` , sebua tag anchor (`<a>`) akan ter-_render_ pada HTML di aplikasimu.

```javascript
<Link to="/">Home</Link>

// <a href="/">Home</a>
```

Komponen `<Navlink>` merupakan tipe spesial dari `<Link>` yang men-_style_ dirinya sendiri sebagai _"active"_ ketika prop _to_ cocok dengan lokasi saat ini.

```javascript
// lokasi = { pathname: '/react' }
<NavLink to="/react" activeClassName="hurray">
  React
</NavLink>
// <a href='/react' className='hurray'>React</a>
```

Kapanpun kamu ingin pindah navigasi secara paksa, kamu bisa me-_render_ `<Redirect>`. Ketika itu ter-_render_ maka akan menavigasi menggunakan propnya.

```javascript
<Redirect to="/login" />
```

===============


## **Basic Routing**
Ada 2 tipe pada komponen Router yang bisa digunakan pada aplikasi web React, yaitu `BrowserRouter` dan `HashRouter`.  Yang pertama (BrowserRouter) memberi anda URL tanpa `#`, dan yang satunya memberimu URL dengan `#` .

**Note:** Jika kamu membangun aplikasi web yang mendukung browser lawas, maka direkomendasikan untuk menggunakan `HashRouter`.

Buka `src/index.js` milikmu dan tambahkan kode dibawah ini:

```javascript
import React from 'react'
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <Router>
    <App />
  </Router>, document.getElementById('root'));
registerServiceWorker();
```

Pada kode di atas, kita telah mengimpor komponen `BrowserRouter`, `Route` dan `Link` dari `react-router-dom`. Dan kita telah membungkus komponen `<App />` dengan `Router`, yang mana `Router` itu adalah _alias_ dari `BrowserRouter`.

Komponen Router adalah langkah pertama untuk melakukan perutean dengan sukses. Ini berfungsi sebagai wadah untuk setiap komponen route yang lainnya. Selain itu, Komponen Router _hanya bisa_ memiliki satu anak elemen atau satu komponen. Sekarang bagaimana kita mendefinisikan rute kita?

Buka `src/App.js` . Disini kita akan mendefinisikan rute-rute kita.

```javascript
import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import './App.css';


const Home = () => (
  <div>
    <h2> Home </h2>
  </div>
);

const Airport = () => (
  <div>
     <ul>
      <li>Jomo Kenyatta</li>
      <li>Tambo</li>
      <li>Murtala Mohammed</li>
    </ul>
  </div>
);

const City = () => (
  <div>
    <ul>
      <li>San Francisco</li>
      <li>Istanbul</li>
      <li>Tokyo</li>
    </ul>
  </div>
);

class App extends Component {
  render() {
    return (
      <div>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/airports">Airports</Link></li>
          <li><Link to="/cities">Cities</Link></li>
        </ul>

        <Route path="/" component={Home}/>
        <Route path="/airports" component={Airport}/>
        <Route path="/cities" component={City}/>
      </div>
    );
  }
}

export default App;

```

Pada kode di atas, kita memiliki link-link yang seharusnya mengarahkan _user_ ke '`/`', '`/airports`' dan '`/cities`' menggunakan komponen `<Link>`. Masing-masing dari link-link ini telah memiliki komponen yang harus diberikan setlah lokasi saat ini cocok dengan jalur rute. Bagaimanapun ada yang tidak beres disini. Mari kita cek hasilnya. 

![Airports route](notexact.png)
_Airports route_

`Home`, yang mana adalah UI untuk komponen `Home` seharusnya hanya memberikan ' `/` ' sebagai rute utama. Namun, halamannya menampilkan semua rute(route).  ' `/` ' cocok dengan ' `/airports` ' dan ' `/cities` ', karena itu rute ' `/airports` ' juga me-_render_ ' `/` ' .

Solusinya cukup mudah, dengan menambahkan _prop_ `exact` ke rute ' `/` ' .

_src.App.js_
```javascript
<Route path="/" exact component={Home}/>
<Route path="/airports" component={Airport}/>
<Route path="/cities" component={City}/>
```

![exact prop](exact.png)

_Rute Airports tanpa me-render komponen Home pada UI_


Pada contoh di atas, semua komponen `<Route />` memiliki _prop_  `component` yang me-_render_ sebuah komponen ketika URL yang  dikunjungi cocok dengan jalur(path) Route. Bagaimana jika kamu hanya ingin me-_render_ sebuah fungsi kecil sebagai ganti dari semua komponen?  Maka kamu bisa menggunakan prop `render` seperti yang ditampilkan pada kode di bawah.

```javascript
<Route path='/airports'
        render={() => (<div> This is the airport route </div>)} />
```


===============



# Meta
kata kunci:
- Routing
- React Router
- BrowserRouter
- Route
- Link
- NavLink
- Switch
- Routing
- Render

tautan:
- [React Training](https://reacttraining.com/react-router/web/guides/basic-components)
- [Auth0](https://auth0.com/blog/react-router-4-practical-tutorial/)

# Latihan
Buatlah project aplikasi react baru dan beri nama "Latihan-Routing" kemudian buat 4 komponen pada aplikasi tersebut, yaitu:
1. Beranda
2. Profil
3. Data-Data
4. Detail Data

Buat Navbar yang mencantumkan 3 Komponen teratas sebagai Rute utama. Di dalam Komponen _**Data-Data**_ terdapat beberapa list data dan list-list tersebut akan ternavigasi ke komponen _**Detail Data**_.

Jika sudah selesai selanjutnya jelaskan alur navigasi dari rute yang anda buat.