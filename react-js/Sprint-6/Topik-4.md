# Kompetensi
- Mengetahui kelas-kelas yang ada pada framework styling.
- Dapat mengaplikasikannya ke dalam project nyata

# Materi

## Styling Component

Di luar sana sudah banyak bertebaran framework-framework yang membuat pekerjaan seorang programmer menjadi lebih mudah dan cepat, framework styling salah satunya. Framework styling adalah sebuah framework yang digunakan untuk menghias atau memperindah User Interface dari sebuah website.

Framework styling ini dapat mempermudah seseorang untuk memperindah tampilan website yang dibuatnya. Dimana framework ini telah menyediakan berbagai macam _class_ CSS yang telah dibuat guna mempercepat pembuatan UI yang menarik.

Pengguna hanya perlu memanggunakan _class_ CSS yang telah ada pada framework tersebut tanpa harus membuat CSS style sendiri. Mudah bukan?

Terdapat banyak framework styling yang ada, seperti: Bootstrap, Materialize CSS dan UI Kit. Namun yang paling populer adalah framework Bootstrap. Tapi yang akan dibahas kali ini adalah framework Materialize CSS, karena bisa bilang framework ini memiliki style yang cukup bagus ketimbang style yang dimiliki Bootstrap.





## **Materialize CSS**

![materialize css](materialize.png)

Untuk mulai menggunakan framework styling ini ada berbagai cara, yang pertama kita bisa mendownload filenya secara langsung dan kita masukan ke dalam project yang sedang kita buat dan yang kedua menggunakan kita bisa menggunakan link CDN tanpa harus mendownload filenya.

Jika kita bekerja secara online maka menggunakan link CDN adalah pilihan yang baik, karena kita tidak perlu repot mendownload dan memasukkannya ke dalam project kita. Namun jika kita ingin bekerja secara offline maka mendownload file dan memasukkannya ke dalam project kita adalah pilihan yang tepat.

Kita akan menggunakan link CDN untuk mempercepat proses penggunaan framework ini. Ynag perlu dilakukan hanya perlu menyalin link-nya dan menempelkannya di file html kita.

Pertama-tama kita menuju web [Materialize CSS](https://materializecss.com) untuk memulai

```html
<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
```

![menu mcss](mcss-menu.png)

Seperti yang kita lihat pada gambar di atas, Materialize CSS memiliki banyak fitur yang dapat langsung digunakan pada project web dan kita akan mulai dari *CSS*.

## **Color**

![css color](css_color.png)

Materialize CSS telah menyiapkan banyak sekali pilihan warna yang dapat kita gunakan pada project kita yang, kita lakukan hanya menggunakan nama _class_ pada elemen HTML yang kita ingin =beri warna.

**Grid**

Materialize menggunakan standar _12 column fluid responsive grid system_. Dimana grid ini akan membantu dalam menuyusun urutan tata letak tampilan pada halaman web kita.

- **Container**
kelas _container_ tidak sepenuhnya merupakan bagian dari grid sistem, tapi kelas ini cukup penting dalam menyusun konten. Kelas ini memungkinkanmu untuk menaruh konten halamanmu di tengah tampilan. Kelas ini diatur sampai 70% dari lebar window. Kita menggunakan kelas ini sebagai wadah dari konten kita. Cukup tambahkan kelas _container_ pada elemen pembungkus kita.

Mari lihat bagaimana Grid bekerja.

**12 Kolom**

Standar grid milik materialize memiliki 12 kolom. Ukuran browser tidaklah penting karena setiap kolom selalu memiliki lebar yang sama.

Ketika kita akan membuat kolom maka semua kolom harus berada pada pembungkus **_row_**.

**Responsive Layout**

Berikut ini adalah cara bagaimana merancang tata letak kita sehingga terlihat bagus di semua ukuran layar.

![responsive](responsive-layout.png)

### **Helpers**

Di dalam menu ini sudah ada tentang **Alignment**, **Hiding and Showing Content** dan **Formatting**. Alignment dapat kita gunakan untuk meluruskan konten kita.

### **Shadow**

Kita tidak perlu lagi repot-repot membuat box-shadow yang memiliki beberapa value untuk memberikan efek bayangan pada konten kita. Kita cukup memanggil kelas yang telah disediakan oleh materialize dan mengatur seberapa besar shadow yang kita inginkan menggunakan kelas `z-depth`.

## **Components**

Dalam menu ini terdapat banyak kelas untuk membuat komponen-kompopen web seperti; Navbar, Button, Footer dll. Semua sudah ada contoh pemakaiannya.

## **Javascript**

Dalam menu Javascript terdapat berbagai komponen yang disertai dengan sedikit animasi yang dihasilkan dari kode javascript.



# Meta

tautan :
- [Materialize CSS](https://materializecss.com)

# Latihan
Buatlah web profile yang mendeskripsikan tentang diri kamu dengan tampilan UI yang sebagus dan semenarik mungkin menggunakan framework materialize css.