# Kompetensi
- Dapat menyebutkan component lifecycle dan kegunaanya
- Dapat membedakan antara Presentational Component dengan Container Component

# Materi
## **Component Lifecycle**
Ketika mendevelop aplikasi react, setiap komponen mengikuti siklus dari saat komponen itu dibuat dan dipasang pada DOM hingga saat dilepas dan dihancurkan. Inilah yang disebut siklus hidup komponen (Component Lifecycle).

React menawarkan **Hooks**, sebuah method yang dapat terpanggil secara otomatis di tiap titik dalam lifecycle, yang bisa memberikanmu kontrol yang baik tentang apa yang terjadi pada titik itu dipanggil. Pemahaman yang baik pada **hooks** ini akan memberikanmu _Power of effectively control_ dan memanipulasi apa yang terjadi pada komponen selama masa hidupnya.

Dalam sebuah komponen react terdapat 3 lifecycle, yaitu :
- Inisialisasi/Mounting
- Update/re-render
- Unmounting

### **1. Inisialisasi / Mounting**
Sebuah komponen terpasang(**mount**) ketika komponen itu terbuat dan pertama kali dimasukkan ke dalam DOM, contohnya saat komponen ter-_render_ pertama kali.
Terdapat 4 method yang akan dieksekusi pada fase ini, metode-metode ini dieksekusi sesuai urutan :
1. constuctor
2. componentWillMount
3. render
4. componentDidMount


- **Constructor** dan **componentWillMount()**
Kedua method tersebut adalah method yang dipanggil tepat sebelum komponen dimuat(mount) atau sebelum method _render_ dieksekusi. 

**componentWillMount** berada diantara method **constructor** dan method **render** yang menempatkannya pada posisi yang sangat aneh. Karena Karena componentWillMount terletak sebelum _render_ method jadi ia bisa digunakan untuk mengatur pengaturan default untuk komponennya.

Cara terbaik untuk menggunakan method **componentWillMount** adalah untuk melakukan pengaturan apapun yang dapat dilakukan saa _runtime_, misalnya untuk mengoneksikan ke API seperti Firebase.

contoh:
```javascript
class Example extends React.Component {
    componentWillMount() {
        console.log('I am about to say hello');
    }

    render() {
        return <h1>Hello world</h1>;
    }
}
```

- **Render** akan mengembalikan sebuah elemen react yang merupakan representasi dari DOM komponen

- **componentDidMount**
Method ini akan tersedia setelah komponen dimuat, yaitu setelah HTML selesai di-_render_. Method ini dapat mengakses dan memanipulasi DOM atau melakukan proses pengambilan data atau AJAX request, jQueryjuga dapat kita panggil dalam method ini.

Ini adalah tempat terbaik meletakkan API, karena pada point ini komponen telah dimuat and telah ada di DOM.

contoh:
```javascript
class Example extends React.Component {
        componentDidMount() {
            fetch(url).then(results => {
                // Lakukan sesuatu dengan hasilnya
            })
        }
    }
```

----------
### 2. Update/re-render
Merupakan fase dimana komponen akan dirender ulang. Proses render ulang dapat dipicu dari perubahan props dan state pada komponen tersebut. Ada 5 method yang akan dieksekusi:
- componentWillReceiveProps
- shouldComponentUpdate
- componentWillUpdate
- render
- componentDidUpdate


1. **componentWillReceiveProps**  

Method ini akan dieksekusi ketika terdapat perubahan props dan bukan merupakan render pertama kali. Method ini dapat digunakan untuk mengubah state sesuai dengan props yang mau diterima. Perubahan state pada method ini tidak akan merender komponen. Oleh karena itu, ketika menggunakan ini fungsi ini kita bisa membandingkan prop yang sekarang dengan prop yang baru dan memeriksa apakah ada sesuatu yang berubahh

contoh:
```javascript
class Example extends React.Component {
  constructor(props) {
    super(props);
    this.state = {number: this.props.number};
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.number !== nextProps.number) {
      this.setState({number: nextProps.number});
    }
  }

  render() {
    return (
      <h1>{this.state.number}</h1>
    )
  }
}

```

Pada contoh diatas `this.state.number` hanya akan ter-update jika nomer yang baru berbeda dengan yang sebelumnya. Jadi jika tidak ada perubahan maka `state` tidak akan ter-update.


2. **shoulComponentUpdate**

Method ini selalu dieksekusi sebelum render. method ini merupakan penentu apakah sebuah komponen akan dirender ulang atau tidak. Method ini mengembalikan nilai true atau false dengan true sebagai nilai defaultnya.

Method ini digunakan untuk memberi tahu React bahwa output komponen tidak berpengaruh oleh perubahan props atau state dalam komponen dan karenanya tidak harus di-render ulang.

Cara terbaik untuk menggunakan method ini adalah untuk me-return nilai false dan karena hal itu maka komponen tidak akan memperbarui dalam kondisi tertentu. Jika kondisi tersebut terpenuhi, maka komponen tidak diperbarui.

Pada contoh di bawah, komponen hanya akan memperbarui jika sebuah inputan baru berbeda dari yang sebelumnya.

```javascript
class Example extends React.Component {
  [...]

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.input == nextState.input) {
      return false;
    }
  }

  [...]
}
```

3. **componenetWillUpdate**

Method ini dieksekusi jika method shouldComponentUpdate berniali true. Pada method ini kita tidak dianjurkan unruk mengubah state, dengan alasan bahwa method ini digunakan untuk mempersiapkan proses update komponen, bukan unutk memicu proses update komponen.

Satu hal utama yang bisa dilakukan oleh method ini adalah untuk berinteraksi dengan hal-hal diluar React. Dan juga jika kamu melakukan pengaturan non-React sebelum komponen me-render seperti berinteraksi dengan API atau memeriksa ukuran window maka **componentWillUpdate** bisa digunakan.

contoh:
```javascript
class Example extends React.Component {
  [...]

  componentWillUpdate(nextProps, nextState) {
      // Do something here
  }

  [...]
}
``` 

4. **componentDidUpdate**

Method ini akan dieksekusi setelah render HTML telah selesai dan kegunaannya sama seperti componentDidUpdate.

contoh:

contoh:
```javascript
class Example extends React.Component {
  [...]

  componentDidUpdate(prevProps, prevState) {
    if (this.props.input == prevProps.input) {
      // make ajax calls
      // Perform any other function
    }
  }

  [...]
}
```

----------

### 3. **Unmounting**
Pada proses unmounting hanya ada satu method yang akan dieksekusi, yaitu componentWillUnmount. Method ini dieksekusi sebelum komponen dihapus atau dihilangkan dari DOM.

Komponen tidak selamanya tinggal dalam DOM. Terkadang mereka harus dihapus karena perubahan pada state atau hal lainnya. Method ini akan membantu kita untuk menangani unmounting komponen.

Method ini dapat melakukan pembersihan apa saha yang haus dilakukan seperti membatalkan timer, membatalkan request network, menghapus event listener atau membatalkan apapun yang telah dibuat di componentDidMount.

contoh :
```javascript
class Example extends React.Component {  
  [...]

  componentWillUnmount() {
      document.removeEventListener("click", SomeFunction);
  }

  [...]
}
```

----------

## **Presentational Component dan Container Component**
Kamu akan mendapati komponen-komponenmu lebih mudah untuk digunakan kembali jika kamu membagi komponen-komponen tersebut ke dalam **2 kategori**. Kategori tersebut adalah *Container* dan *Presentational* Component, banyak istilah-istilah lainnya seperti: *Fat and Skinny*, *Smart and Dumb*, *Stateful and Pure*, *Screens and Components*, dan lain-lain.

### **Presentational** Components
- Lebih perhatian terhadap *how things look*.
- Mungkin berisi presentational dan container komponen di dalamnya dan biasanya memiliki beberapa DOM dan gaya(style) nya sendiri.
- Lebih sering mengizinkan *containment* melalui *this.props.children*
- Tidak memiliki dependencies pada sisa aplikasi, seperti action redux atau store nya.
- Jangan tentukan bagaimana data itu termuat atau termutasi.
- Menerima data dan *callback* secara khusus melalui props.
- Jarang memiliki state sendiri, bila memiliki state maka itu hanya UI bukan data.
- Tertulis sebagai *functional component*
- Contoh: *Page, Sidebar, Story, UserInfo, List.*

### **Container Components**
- Lebih Perhatian terhadap *how things work*.
- Mungkin berisi Presentational dan Container komponen di dalamnya, tapi biasanya tidak memiliki DOM sendiri kecuali untuk beberapa div pembungkus dan tidak pernah memiliki  gaya(style).
- Memberikan data dan behavior ke komponen presenational atau komponen lainnya.
- Memanggil actions Redux dan memberikan itu sebagai callback ke komponen presentational.
- Lebih sering *stateful*, karena cenderung berfungsi sebagai sumber data.
- Biasanya dihasilkan menggunakan komponen dengan urutan lebih tinggi seperti *connect()* dari React Redux, *createContainer()* dari Relay, atau *Container.create()* dari Flux Utils, daripada ditulis dengan tangan.
- Contoh: *UserPage, FollowersSidebar, StoryContainer, FollowedUserList.*

### **Keuntungan menggunakan pendekatan dengan cara ini**
- Lebih baik dalam pemisahan perhatian. Kamu dapat mengerti  aplikasimu dan UI mu lebih baik dengan menuliskan komponen dengan cara ini.
- Reusability yang lebih baik. Kamu dapat menggunakan komponen presentational yang sama dengan sumber state yang sama sekali berbeda dan mengubahnya menjadi komponen yang dapat digunakan kembali.
- Komponen presentational pada dasarnya adalah 'palet' aplikasimu. Kamu dapat menempatkannya di satu halaman dan membiarkan desainer men-tweak semua variasi mereka tanpa menyentuh logika aplikasinya.
- Hal ini memaksa anda untuk mengekstrak "layout components" seperti *Sidebar, Page, ContextMenu* dan menggunakan *this.props.children* sebagai ganti dari menduplikasi markup dan layout yang sama di beberapa komponen container.


# Meta
kata kunci:
- Hooks
- Props
- UI
- Callback

tautan :
- [Component Lifecycle](https://blog.pusher.com/beginners-guide-react-component-lifecycle/)
- [Presentational and Container Components](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0)


# Latihan
Kapan harus memperkenalkan Container?